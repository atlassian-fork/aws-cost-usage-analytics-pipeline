COST_REPORT_OLD = LOAD '$OLD_BILLING_FILES' USING org.apache.pig.piggybank.storage.CSVExcelStorage(',', 'YES_MULTILINE', 'NOCHANGE', 'SKIP_INPUT_HEADER') as ($OLD_COST_REPORT_INPUT_SCHEMA);
COST_REPORT_NEW = LOAD '$NEW_BILLING_FILES' USING org.apache.pig.piggybank.storage.CSVExcelStorage(',', 'YES_MULTILINE', 'NOCHANGE', 'SKIP_INPUT_HEADER') as ($NEW_COST_REPORT_INPUT_SCHEMA);

COST_REPORT_COGROUP = cogroup COST_REPORT_OLD by (identity::LineItemId,identity::TimeInterval) OUTER, COST_REPORT_NEW by (identity::LineItemId,identity::TimeInterval);
RESULT = foreach (FILTER COST_REPORT_COGROUP by IsEmpty(COST_REPORT_OLD)) generate flatten(COST_REPORT_NEW);
FORMATTED_RESULT = FOREACH RESULT GENERATE $COST_REPORT_OUTPUT_SCHEMA;

store FORMATTED_RESULT into '$OUTPUT' USING org.apache.pig.piggybank.storage.CSVExcelStorage(',', 'NO_MULTILINE', 'UNIX', 'SKIP_OUTPUT_HEADER');
