#!/bin/bash

INPUT=$1
OUTPUT=$2
RAND=$(cat /dev/urandom | base64 | tr -dc 'a-zA-Z0-9' | fold -w 16 | head -n 1)

for file in $(aws s3 ls $INPUT|awk '{ print $NF }')
do
  aws s3 cp $INPUT$file $OUTPUT$RAND-$file
done
