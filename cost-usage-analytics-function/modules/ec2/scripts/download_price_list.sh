#!/bin/bash

curl https://pricing.us-east-1.amazonaws.com/offers/v1.0/aws/AmazonEC2/current/index.csv | sed -n '/\"SKU\",\"OfferTermCode\"/,$p' > price_list.csv
aws s3 cp price_list.csv $1

